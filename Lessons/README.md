The links below are to static renderings of the notebooks via [nbviewer.ipython.org](http://nbviewer.ipython.org/).
Descriptions below the links are from the first cell of the notebooks (if that cell contains Markdown or raw text).

* ##[PoincareContinuationMethod.ipynb](http://nbviewer.jupyter.org/urls/bitbucket.org/msanrivo/periodicas/raw/f2d7310c73e1d1daa3d935b3e84d98900360faf2/Lessons/Predictor-Corrector%20Algorithm.ipynb) 